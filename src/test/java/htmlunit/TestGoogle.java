package htmlunit;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.camilo.htmlunit.entities.Busqueda;
import com.camilo.htmlunit.interfases.dao.UIBusquedaDAO;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.DomNode;
import com.gargoylesoftware.htmlunit.html.HtmlElement;
import com.gargoylesoftware.htmlunit.html.HtmlPage;

//@RunWith(SpringJUnit4ClassRunner.class)
//@ContextConfiguration({"classpath*:META-INF/test-context.xml"})
public class TestGoogle {

	private final static Logger log = Logger.getLogger(TestGoogle.class);

	@Autowired
	private UIBusquedaDAO busquedaDao;

	@Test
	public void pageGoogle() throws Exception {
		List<Busqueda> result = busquedaDao.consultarBusquedasRelacion("avi");

		Assert.assertNotNull(result);
	}

	@Test
	public void consultarBusquedasRelacion() throws Exception {
		String palabra = "avi";
		WebClient webClient = new WebClient();
		ArrayList<Busqueda> resultBusqueda = new ArrayList<Busqueda>();

		HtmlPage currentPage = webClient
				.getPage("https://www.google.com/search?q=" + palabra.trim());

		/*
		 * if (currentPage.getByXPath("//h3").size() == 0) { throw new
		 * Exception("No hay datos a mostrar"); }
		 */
		for (int i = 0; i < currentPage.getByXPath("//h3").size(); i++) {
			HtmlElement element = (HtmlElement) currentPage.getByXPath("//h3")
					.get(i);
			DomNode urlBusqueda = element.getChildNodes().get(0);
			Busqueda resultElement = new Busqueda();
			resultElement.setDatoBusqueda(urlBusqueda.getTextContent());
			resultElement.setResultBusqueda(urlBusqueda.getAttributes()
					.getNamedItem("href").getNodeValue());
			resultBusqueda.add(resultElement);
		}

		System.out.println(resultBusqueda.toString());

	}

}
