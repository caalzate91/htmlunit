package com.camilo.htmlunit.interfases.services;

import java.util.List;

import com.camilo.htmlunit.entities.Busqueda;

/**
 * @author <a href="mailto:caalzate91@gmail.com">Camilo Alzate</a>
 * @project api-htmlunit
 * @class UIBusquedaService
 * @since 30/05/2016
 *
 */

public interface UIBusquedaService {

	/**
	 * @author <a href="mailto:caalzate91@gmail.com">Camilo Alzate</a>
	 * @since 30/05/2016
	 * @description Metodo para obtener busquedas relacionadas con la palabra a
	 *              digitar
	 * @param palabra
	 */
	List<Busqueda> consultarBusquedasRelacion(String palabra) throws Exception;
}
