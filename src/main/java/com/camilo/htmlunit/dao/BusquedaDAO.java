package com.camilo.htmlunit.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.camilo.htmlunit.entities.Busqueda;
import com.camilo.htmlunit.interfases.dao.UIBusquedaDAO;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.DomNode;
import com.gargoylesoftware.htmlunit.html.HtmlElement;
import com.gargoylesoftware.htmlunit.html.HtmlPage;

/**
 * @author <a href="mailto:caalzate91@gmail.com">Camilo Alzate</a>
 * @project api-htmlunit
 * @class BusquedaDAO
 * @since 30/05/2016
 *
 */
@Repository
public class BusquedaDAO implements UIBusquedaDAO {

	/**
	 * @author <a href="mailto:caalzate91@gmail.com">Camilo Alzate</a>
	 * @since 30/05/2016
	 * @see com.camilo.htmlunit.interfases.dao.UIBusquedaDAO#consultarBusquedasRelacion(String
	 *      palabra)
	 */
	@Override
	public List<Busqueda> consultarBusquedasRelacion(String palabra)
			throws Exception {
		WebClient webClient = new WebClient();
		ArrayList<Busqueda> resultBusqueda = new ArrayList<Busqueda>();

		HtmlPage currentPage = webClient
				.getPage("https://www.google.com/search?q=" + palabra.trim());

		for (int i = 0; i < currentPage.getByXPath("//h3").size(); i++) {
			HtmlElement element = (HtmlElement) currentPage.getByXPath("//h3")
					.get(i);
			DomNode urlBusqueda = element.getChildNodes().get(0);
			Busqueda resultElement = new Busqueda();
			resultElement.setDatoBusqueda(urlBusqueda.getTextContent());
			resultElement.setResultBusqueda(urlBusqueda.getAttributes()
					.getNamedItem("href").getNodeValue());
			resultBusqueda.add(resultElement);
		}
		return resultBusqueda;
	}

}
