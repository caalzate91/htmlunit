package com.camilo.htmlunit.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.camilo.htmlunit.entities.Busqueda;
import com.camilo.htmlunit.interfases.dao.UIBusquedaDAO;
import com.camilo.htmlunit.interfases.services.UIBusquedaService;

/**
 * @author <a href="mailto:caalzate91@gmail.com">Camilo Alzate</a>
 * @project api-htmlunit
 * @class BusquedaServices
 * @since 30/05/2016
 *
 */
@Service("busquedaService")
public class BusquedaService implements UIBusquedaService {

	@Autowired
	private UIBusquedaDAO busquedaDAO;

	@Override
	public List<Busqueda> consultarBusquedasRelacion(String palabra)
			throws Exception {

		if (palabra.isEmpty()) {
			throw new Exception("El criterio de busqueda est� vacio");
		}

		List<Busqueda> result;

		try {
			result = busquedaDAO.consultarBusquedasRelacion(palabra);
		} catch (Exception e) {
			throw new Exception(e.getMessage(), e.getCause());
		}
		return result;
	}

}
