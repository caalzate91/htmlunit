package com.camilo.htmlunit.util;

import java.io.Serializable;
import java.util.Map;

/**
 * @author <a href="mailto:caalzate91@gmail.com">Camilo Alzate</a>
 * @project api-htmlunit
 * @class JsonResponse
 * @description
 * @date 30/05/2016
 */
public class JsonResponse<T> implements Serializable {

	private static final long serialVersionUID = -3119714075845042171L;
	public static final Integer STATUS_SUCCESS = 200;
	public static final Integer STATUS_ERROR = 500;

	private Integer status;
	private T result;
	private Boolean success;
	private Object errorCode;
	private Object exceptionMessage;
	private Object severity;
	private Map<String, Object> additionalData;

	public JsonResponse() {
	}

	public JsonResponse(Integer status, T result) {
		super();
		this.status = status;
		this.result = result;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Object getResult() {
		return result;
	}

	public void setResult(T result) {
		this.result = result;
	}

	public Boolean getSuccess() {
		return success;
	}

	public void setSuccess(Boolean success) {
		this.success = success;
	}

	public Object getExceptionMessage() {
		return exceptionMessage;
	}

	public void setExceptionMessage(Object exceptionMessage) {
		this.exceptionMessage = exceptionMessage;
	}

	public Map<String, Object> getAdditionalData() {
		return additionalData;
	}

	public void setAdditionalData(Map<String, Object> additionalData) {
		this.additionalData = additionalData;
	}

	public Object getSeverity() {
		return severity;
	}

	public void setSeverity(Object severity) {
		this.severity = severity;
	}

	public Object getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(Object errorCode) {
		this.errorCode = errorCode;
	}

}
