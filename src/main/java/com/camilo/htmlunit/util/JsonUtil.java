package com.camilo.htmlunit.util;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * @author <a href="mailto:caalzate91@gmail.com">Camilo Alzate</a>
 * @project api-htmlunit
 * @class JsonUtil
 * @description
 * @date 30/05/2016
 */
public final class JsonUtil {

	private JsonUtil() {
	}

	public static String toJson(Object value) {
		Gson gson = new Gson();
		return gson.toJson(value);
	}

	public static String toJson(Object value, String dateFormat) {
		Gson gson = new GsonBuilder().setDateFormat(dateFormat).create();
		return gson.toJson(value);
	}

	public static <T> T fromJson(String json, Class<T> type) {
		Gson gson = new Gson();
		return gson.fromJson(json, type);
	}

	public static <T> T fromJson(String json, String dateFormat, Class<T> type) {
		Gson gson = new GsonBuilder().setDateFormat(dateFormat).create();
		return gson.fromJson(json, type);
	}
}
