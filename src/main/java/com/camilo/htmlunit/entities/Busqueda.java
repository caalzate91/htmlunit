package com.camilo.htmlunit.entities;

/**
 * @author <a href="mailto:caalzate91@gmail.com">Camilo Alzate</a>
 * @project api-htmlunit
 * @class Busqueda
 * @since 30/05/2016
 *
 */

public class Busqueda {
	
	private String datoBusqueda;
	private String resultBusqueda;
	
	
	public Busqueda() {
	}


	public String getDatoBusqueda() {
		return datoBusqueda;
	}


	public void setDatoBusqueda(String datoBusqueda) {
		this.datoBusqueda = datoBusqueda;
	}


	public String getResultBusqueda() {
		return resultBusqueda;
	}


	public void setResultBusqueda(String resultBusqueda) {
		this.resultBusqueda = resultBusqueda;
	} 

	
	
}
