package com.camilo.htmlunit.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.camilo.htmlunit.entities.Busqueda;
import com.camilo.htmlunit.util.JsonResponse;
import com.camilo.htmlunit.util.JsonUtil;

@Controller
@RequestMapping
public class BusquedaController {

	private static final Logger LOG = Logger
			.getLogger(BusquedaController.class);

	/*@Autowired
	private BusquedaService busquedaService;*/

	/***
	 * 
	 * @author <a href="mailto:camilo.alzate@premize.com">Camilo Alzate</a>
	 * @since 20/10/2015
	 * @param request
	 * @param session
	 * @return
	 */
	@RequestMapping(value = "/busqueda", method = RequestMethod.GET, produces = "application/json; charset=utf-8")
	@ResponseBody
	public String getPeriodoAcademico(@RequestParam Map<String, String> params,
			HttpServletRequest request, HttpSession session) {

		JsonResponse<List<Busqueda>> jsonResponse = new JsonResponse<>();

		// Intentamos traer los registros
		try {

			//List<Busqueda> list = busquedaService.consultarBusquedasRelacion(params.get("palabra"));
			/*List<Busqueda> list = busquedaService
					.consultarBusquedasRelacion("avi");*/

			// Convertimos los resultados en tipo JSON, y enviamos los datos del
			// status y del completado del resultado.
			//jsonResponse.setResult(list);
			jsonResponse.setStatus(JsonResponse.STATUS_SUCCESS);
			jsonResponse.setSuccess(true);

		}

		// Capturamos una excepcion general
		catch (Exception ex) {
			LOG.error(ex.getMessage(), ex);
			jsonResponse.setStatus(JsonResponse.STATUS_ERROR);
			jsonResponse.setSuccess(false);
			jsonResponse.setExceptionMessage(ex);
		}

		return JsonUtil.toJson(jsonResponse);

	}
	
	@RequestMapping(value = "/pruebaDatos", method = RequestMethod.GET, produces = "application/json; charset=utf-8")
	@ResponseBody
	public String getPruebaDatos(@RequestParam Map<String, String> params,
			HttpServletRequest request, HttpSession session) {

		JsonResponse<List<Busqueda>> jsonResponse = new JsonResponse<>();

		// Intentamos traer los registros
		try {
			
			ArrayList<Busqueda> prueba = new ArrayList<Busqueda>();
			
			Busqueda dato1 = new Busqueda();
			dato1.setDatoBusqueda("AVI - Wikipedia, la enciclopedia libre");
			dato1.setResultBusqueda("https://es.wikipedia.org/wiki/AVI");
			
			Busqueda dato2 = new Busqueda();
			dato2.setDatoBusqueda("Conversor de v�deos a AVI - convierte tus v�deos a AVI");
			dato2.setResultBusqueda("http://video.online-convert.com/es/convertir-a-avi");
			
			Busqueda dato3 = new Busqueda();
			dato3.setDatoBusqueda("AVI | DivX.com");
			dato3.setResultBusqueda("http://www.divx.com/es/software/technologies/avi");
			
			prueba.add(dato1);
			prueba.add(dato2);
			prueba.add(dato3);

			//List<Busqueda> list = busquedaService.consultarBusquedasRelacion(params.get("palabra"));
			List<Busqueda> list = prueba;

			// Convertimos los resultados en tipo JSON, y enviamos los datos del
			// status y del completado del resultado.
			jsonResponse.setResult(list);
			jsonResponse.setStatus(JsonResponse.STATUS_SUCCESS);
			jsonResponse.setSuccess(true);

		}

		// Capturamos una excepcion general
		catch (Exception ex) {
			LOG.error(ex.getMessage(), ex);
			jsonResponse.setStatus(JsonResponse.STATUS_ERROR);
			jsonResponse.setSuccess(false);
			jsonResponse.setExceptionMessage(ex);
		}

		return JsonUtil.toJson(jsonResponse);

	}
}
