<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Bootstrap 3, from LayoutIt!</title>

    <meta name="description" content="Source code generated using layoutit.com">
    <meta name="author" content="LayoutIt!">

    <link href="resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="resources/css/style.css" rel="stylesheet">

  </head>
  <body>

    <div class="container-fluid">
    <nav>
		<div class="row">
			<div class="col-md-5">
				<img alt="Bootstrap Image Preview" src="http://lorempixel.com/300/140/" class="img-rounded">
			</div>
			<div class="col-md-7 text-right">
				<h1>Prueba de Desarrollo</h1>
			</div>
		</div>
		<br>
    </nav>
	<br>
	<div class="row">
		<aside>
			<div class="col-md-6">
				<h1>
					Formulario
				</h1>
				<form>
					<p>Por favor poner la palabra a buscar:</p>
				   <div class="form-group">
					    <label for="inputPalabra">Palabra</label>
					    <input type="text" class="form-control" id="inputPalabra" placeholder="Palabra">
				    </div>
					<a class="btn btn-primary">Submit</a>
				</form>
		</aside>
		<aside>
			<div class="col-md-6">
				<h1>
					Resultado de busquedas relacionadas:
				</h1>
				<div id="resultado">
						
				</div>
		</aside>
	</div>	
	<br>	
	<footer>
		<div class="row">
			<div class="col-md-12">
				<p class="text-center">
					Realizado por <strong>Camilo Alzate</strong>. <br>Aliquam eget sapien sapien. Curabitur in metus urna. 
				</p>
			</div>
		</div>
	</footer>
</div>

    <script src="resources/js/jquery.min.js"></script>
    <script src="resources/js/bootstrap.min.js"></script>
    <script src="resources/js/scripts.js"></script>
  </body>
</html>